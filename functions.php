<?php

/*** Child Theme Function  ***/

function eltd_child_theme_enqueue_scripts() {
	wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
	
	wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js');	
	wp_enqueue_style( 'childstyle' );
}

add_action('wp_enqueue_scripts', 'eltd_child_theme_enqueue_scripts', 11);
add_filter('use_block_editor_for_post_type', '__return_false', 100);

/**
 * Function that adds classes on body slug
 */
function eltd_moose_child_classes($classes) {
	if( is_page()) {
	    global $post;
	    $slug = $post->post_name;
	    $classes[] = $slug;
		
		$key_value = get_post_meta( $post->ID, '_necs_page_class', true );
		// Check if the custom field has a value.
		if ( ! empty( $key_value ) ) {
			$classes[] = $key_value;
		}
    } 
	return $classes;
}

add_filter('body_class', 'eltd_moose_child_classes');



/**
 * Create a metabox with a single field for _necs_page_class
 * 
 * */

function _necs_page_class_create_metabox() {

	// To add the metabox to a page, too, you'd repeat it, changing the location
	add_meta_box( '_necs_metabox', 'Page Class', '_necs_page_class_render_metabox', 'page', 'side', 'default'); // Priority relative to other metaboxes );

}
add_action( 'add_meta_boxes', '_necs_page_class_create_metabox' );


/**
 * Render the metabox markup
 * This is the function called in `_necs_page_class_create_metabox()`
 */
function _necs_page_class_render_metabox() {
	// Variables
	global $post; // Get the current post data
	$details = get_post_meta( $post->ID, '_necs_page_class', true ); // Get the saved values
?>

<fieldset>
	<div>
		<label for="_necs_page_class_custom_metabox">
			<?php
	// This runs the text through a translation and echoes it (for internationalization)
	_e( 'Page Class', '_necs_page_class' );
			?>
		</label>
		<?php
	// The `esc_attr()` function here escapes the data for
	// HTML attribute use to avoid unexpected issues
		?>
		<input
			   type="text"
			   name="_necs_page_class_custom_metabox"
			   id="_necs_page_class_custom_metabox"
			   value="<?php echo esc_attr( $details ); ?>"
			   >
	</div>
</fieldset>

<?php
	// Security field
	// This validates that submission came from the
	// actual dashboard and not the front end or
	// a remote server.
	wp_nonce_field( '_necs_page_class_form_metabox_nonce', '_necs_page_class_form_metabox_process' );
}

/**
 * Save the metabox
 * @param  Number $post_id The post ID
 * @param  Array  $post    The post data
 */
function _necs_page_class_save_metabox( $post_id, $post ) {

	// Verify that our security field exists. If not, bail.
	if ( !isset( $_POST['_necs_page_class_form_metabox_process'] ) ) return;

	// Verify data came from edit/dashboard screen
	if ( !wp_verify_nonce( $_POST['_necs_page_class_form_metabox_process'], '_necs_page_class_form_metabox_nonce' ) ) {
		return $post->ID;
	}

	// Verify user has permission to edit post
	if ( !current_user_can( 'edit_post', $post->ID )) {
		return $post->ID;
	}

	// Check that our custom fields are being passed along
	// This is the `name` value array. We can grab all
	// of the fields and their values at once.
	if ( !isset( $_POST['_necs_page_class_custom_metabox'] ) ) {
		return $post->ID;
	}
	/**
	 * Sanitize the submitted data
	 * This keeps malicious code out of our database.
	 * `wp_filter_post_kses` strips our dangerous server values
	 * and allows through anything you can include a post.
	 */
	$sanitized = wp_filter_post_kses( $_POST['_necs_page_class_custom_metabox'] );
	// Save our submissions to the database
	update_post_meta( $post->ID, '_necs_page_class', $sanitized );

}
add_action( 'save_post', '_necs_page_class_save_metabox', 1, 2 );


//
// Save a copy to our revision history
// This is optional, and potentially undesireable for certain data types.
// Restoring a a post to an old version will also update the metabox.
//
/**
	 * Save events data to revisions
	 * @param  Number $post_id The post ID
	 */
function _necs_page_class_save_revisions( $post_id ) {
	// Check if it's a revision
	$parent_id = wp_is_post_revision( $post_id );
	// If is revision
	if ( $parent_id ) {
		// Get the saved data
		$parent = get_post( $parent_id );
		$details = get_post_meta( $parent->ID, '_necs_page_class', true );
		// If data exists and is an array, add to revision
		if ( !empty( $details ) ) {
			add_metadata( 'post', $post_id, '_necs_page_class', $details );
		}
	}
}
add_action( 'save_post', '_necs_page_class_save_revisions' );
/**
	 * Restore events data with post revisions
	 * @param  Number $post_id     The post ID
	 * @param  Number $revision_id The revision ID
	 */
function _necs_page_class_restore_revisions( $post_id, $revision_id ) {
	// Variables
	$post = get_post( $post_id ); // The post
	$revision = get_post( $revision_id ); // The revision
	$details = get_metadata( 'post', $revision->ID, '_necs_page_class', true ); // The historic version
	// Replace our saved data with the old version
	update_post_meta( $post_id, '_necs_page_class', $details );
}
add_action( 'wp_restore_post_revision', '_necs_page_class_restore_revisions', 10, 2 );
/**
	 * Get the data to display on the revisions page
	 * @param  Array $fields The fields
	 * @return Array The fields
	 */
function _necs_page_class_get_revisions_fields( $fields ) {
	// Set a title
	$fields['_necs_page_class'] = 'Page Class';
		return $fields;
}

add_filter( '_wp_post_revision_fields', '_necs_page_class_get_revisions_fields' );

/**
	 * Display the data on the revisions page
	 * @param  String|Array $value The field value
	 * @param  Array        $field The field
	 */
function _necs_page_class_display_revisions_fields( $value, $field ) {
	global $revision;
	return get_metadata( 'post', $revision->ID, $field, true );
}
add_filter( '_wp_post_revision_field_my_meta', '_necs_page_class_display_revisions_fields', 10, 2 );

/**
 * ADD MENU FOOTER
 * 
 **/
function register_footer_menu() {
  register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_footer_menu' );
